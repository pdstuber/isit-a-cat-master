#!/usr/bin/env sh
helm repo add jetstack https://charts.jetstack.io
helm repo add nginx-stable https://helm.nginx.com/stable
helm repo update
kubectl create namespace cert-manager
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v0.15.1 --set installCRDs=true
kubectl create -f certmanager/
helm install nginx-ingress nginx-stable/nginx-ingress --set rbac.create=true --set controller.publishService.enabled=true
helm install minio stable/minio
kubectl create namespace nats-io
kubectl create -f nats/
kubectl create namespace isit-a-cat
kubectl apply -f namespace.yaml
kubectl config set-context --current --namespace=isit-a-cat
kubectl create -f prometheus-operator/
kubectl create -f prometheus/
kubectl create secret generic canister.io --from-file=.dockerconfigjson=canisterio.json --type=kubernetes.io/dockerconfigjson
kubectl create secret generic gcloud-credentials-json --from-file=gcloud-credentials.json=creds.json
kubectl create -f configmap.yaml
kubectl create -f secrets.yaml
kubectl create -f minio/
kubectl create -f ../isit-a-cat-bff/k8s
kubectl create -f ../isit-a-cat-front/k8s
kubectl create -f ../isit-a-cat-predict-go/k8s

