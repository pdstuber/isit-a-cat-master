#!/usr/bin/env sh
kubectl delete -f nats/nats-io.yaml
kubectl delete crd/natsclusters.nats.io
kubectl delete crd/natsserviceroles.nats.io
helm del nginx-ingress
helm del cert-manager 
helm del prometheus-operator
helm del minio
kubectl delete crd/alertmanagers.monitoring.coreos.com
kubectl delete crd/podmonitors.monitoring.coreos.com              
kubectl delete crd/prometheuses.monitoring.coreos.com             
kubectl delete crd/prometheusrules.monitoring.coreos.com                  
kubectl delete crd/servicemonitors.monitoring.coreos.com 
kubectl delete crd/challenges.acme.cert-manager.io
kubectl delete crd/orders.acme.cert-manager.io
kubectl delete crd/clusterissuers.cert-manager.io
kubectl delete -f configmap.yaml
kubectl delete secret canister.io
kubectl delete secret gcloud-credentials-json
kubectl delete -f ../isit-a-cat-bff/k8s
kubectl delete -f ../isit-a-cat-front/k8s
kubectl delete -f ../isit-a-cat-predict/k8s
kubectl delete -f nats/nats.yaml
kubectl delete -f nats/tls.yaml
kubectl delete -f prom_role_binding.yaml
kubectl delete -f prom_role.yaml
kubectl delete -f prom_service_account.yaml
kubectl delete -f prometheus.yaml
kubectl delete -f namespace.yaml
kubectl delete -f secrets.yaml
kubectl delete namespace nats-io
kubectl delete namespace monitoring
