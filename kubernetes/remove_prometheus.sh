#!/usr/bin/env sh
helm del --purge prometheus-operator
kubectl delete crd/alertmanagers.monitoring.coreos.com
kubectl delete crd/podmonitors.monitoring.coreos.com
kubectl delete crd/prometheuses.monitoring.coreos.com
kubectl delete crd/prometheusrules.monitoring.coreos.com
kubectl delete crd/servicemonitors.monitoring.coreos.com
kubectl -n monitoring delete prometheus/prometheus
